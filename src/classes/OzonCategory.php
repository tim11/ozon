<?php


namespace Jlabs\classes;


use Jlabs\abstracts\OzonBase;

/**
 * Class OzonCategory
 * @package Jlabs\classes
 */
final class OzonCategory extends OzonBase
{
	/**
	 * @var int $id
	 */
	public $id;
	/**
	 * @var string $title
	 */
	public $title;
	/**
	 * @var array $children
	 */
	public $children = [];

	/**
	 * @var int $parent
	 */
	public $parent = 0;
}

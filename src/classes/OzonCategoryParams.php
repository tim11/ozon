<?php


namespace Jlabs\classes;


use Jlabs\abstracts\OzonBase;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property boolean $is_collection;
 * @property boolean $is_required;
 * @property int $group_id;
 * @property string $group_name;
 * @property int $dictionary_id;
 */
final class OzonCategoryParams extends OzonBase
{
	public $id;
	public $title;
	public $description;
	public $is_collection = false;
	public $is_required = false;
	public $group_id = 0;
	public $group_name;
	public $dictionary_id = 0;

}

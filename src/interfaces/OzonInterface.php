<?php


namespace Jlabs\interfaces;


interface OzonInterface
{
	/**
	 * Настройка подключения к Ozon.
	 * @param int $client_id Номер клиента
	 * @param string $api_key API-ключ
	 * @return self
	 */
	public static function create(int $client_id, string $api_key): self;

	/**
	 * Получить категории в виде списка
	 * @param int|null $parent_id ID родителя
	 * @return array
	 */
	public function getCategoryAsList(int $parent_id = null): array;

	/**
	 * Получить категории в виде дерева
	 * @param int|null $parent_id ID родителя
	 * @return array
	 */
	public function getCategoryAsTree(int $parent_id = null): array;

	/**
	 * Получить список характеристик категории
	 * @param int $category_id ID категории
	 * @param bool $only_required Только обязательные
	 * @return array
	 */
	public function getCategoryParams(int $category_id, bool $only_required = false): array;
}

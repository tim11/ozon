<?php


namespace Jlabs\models;


use GuzzleHttp\Client;
use Jlabs\classes\OzonCategory;
use Jlabs\classes\OzonCategoryParams;
use Jlabs\interfaces\OzonInterface;

class V1 implements OzonInterface
{
	const URL_CATEGORY_TREE = "/v1/category/tree";
	const URL_CATEGORY_PARAMS = "/v2/category/attribute";
	/**
	 * @var int $client_id
	 * @var string $api_key
	 * @var Client $client
	 */
	private $client_id;
	private $api_key;
	private $client;

	public function __construct(int $client_id, string $api_key)
	{
		$this->client_id = (int) $client_id;
		$this->api_key = (string) $api_key;
	}

	private function request(string $url, array $data = []): array{
		if (!$this->client){
			$this->client = new Client([
				"base_uri" => "https://api-seller.ozon.ru",
//				"base_uri" => "https://scb-api.ozonru.me",
			]);
		}
		$request = $this->client->request("POST", $url, [
			"json" => $data,
			"headers" => [
				"Client-Id" => $this->client_id,
				"Api-Key" => $this->api_key
			],
			"http_errors" => false,
		]);
		if ((int) $request->getStatusCode() === 200){
			$json = json_decode($request->getBody()->getContents(), true);
			return $json["result"] ?? [];
		}
		return [];
	}

	/**
	 * Создание подключения к Ozon
	 * @param int $client_id Код клиента
	 * @param string $api_key API-Ключ
	 * @return OzonInterface
	 */
	public static function create(int $client_id, string $api_key): OzonInterface
	{
		return new self($client_id, $api_key);
	}

	public function getCategoryAsList(int $parent_id = null): array
	{
		$response = [];
		$params = [
			"language" => "DEFAULT",
			"category_id" => $parent_id ?: 0
		];
		$tree = $this->request(self::URL_CATEGORY_TREE, $params);
		if ($tree){
			$recurse = function (array $children = [], int $parent = 0) use (&$recurse, &$response){
				if (!empty($children)){
					foreach ($children as $child){
						$response[] = new OzonCategory([
							"id" => (int) $child["category_id"],
							"parent" => $parent,
							"title" => (string) $child["title"]
						]);
						if (!empty($child["children"])){
							$recurse($child["children"], (int) $child["category_id"]);
						}

					}
				}
			};
			$recurse($tree);
		}
		return $response;
	}

	public function getCategoryAsTree(int $parent_id = null): array
	{
		$response = [];
		$params = [
			"language" => "DEFAULT",
			"category_id" => $parent_id ?: 0
		];
		$tree = $this->request(self::URL_CATEGORY_TREE, $params);
		if ($tree){
			$recurse = function (array $children = [], int $parent = 0) use (&$recurse){
				$response = [];
				if (!empty($children)){
					foreach ($children as $child){
						$item = new OzonCategory([
							"id" => (int) $child["category_id"],
							"parent" => $parent,
							"title" => (string) $child["title"]
						]);
						if (!empty($child["children"])){
							$item->children = $recurse($child["children"], $item->id);
						}
						$response[] = $item;
					}
				}
				return $response;
			};
			$response = $recurse($tree);
		}
		return $response;
	}

	/**
	 * @param int $category_id
	 * @param bool $only_required
	 * @return OzonCategoryParams[]
	 */
	public function getCategoryParams(int $category_id, bool $only_required = false): array
	{
		$response = [];
		$params = [
			"language" => "DEFAULT",
			"category_id" => $category_id
		];
		if ($only_required){
			$params["attribute_type"] = "required";
		}
		$attributes = $this->request(self::URL_CATEGORY_PARAMS, $params);
		if ($attributes){
			return array_map(
				static function(array $attribute){
					return new OzonCategoryParams([
						"id" => (int) $attribute["id"],
						"title" => (string) $attribute["name"],
						"description" => (string) $attribute["description"],
						"is_collection" => (boolean) $attribute["is_collection"],
						"is_required" => (boolean) $attribute["is_required"],
						"group_id" => (int) $attribute["group_id"],
						"group_name" => (string) $attribute["group_name"],
						"dictionary_id" => (int) $attribute["dictionary_id"]
					]);
				}, $attributes
			);
		}
		return [];
	}
}

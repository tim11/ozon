<?php


namespace Jlabs\abstracts;


use ReflectionClass;

abstract class OzonBase
{
	public function __construct(array $config = []){
		if (count($config)){
			$reflection = new ReflectionClass($this);
			foreach ($config as $key => $value){
				if ($reflection->hasProperty($key)){
					$property = $reflection->getProperty($key);
					if ($property->isPublic()){
						$property->setValue($this, $value);
					}
				}
			}
		}
	}
}
